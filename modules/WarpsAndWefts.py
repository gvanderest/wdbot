from module import Module
import random, threading, time

# This will run a public game (open to anyone) to guess
# a secret word within a set time period. The player that guesses
# correctly will be rewarded.

class WarpsAndWeftsGame():

    # The __init__ method will initialize the game with whatever
    # parameters the calling immortal admin decides.
    # The parameters will be: duration, reward
    # duration is minutes, reward is AP.
    
    def __init__(self, duration, reward):
        # This initializes the game, will be run when a new game object created.
        # In a future version, the lettercount or the word itself will be a parameter.
        # For now, only three letter words. And the duration in minutes and the AP reward.
        self.secret_word = random.choice(words3letters)
        self.Game_Time = int(duration) * 60        
        self.AP_Reward = int(reward)
        self.Playing == True
        self.set_game_time(self, self.Game_Time)
        self.intro()

    def get_word(self):
        # selects a random word from the words3letters tuple.
        print('Selecting a random word.')
        word = random.choice(words3letters)
        return word

    def set_game_time(self, duration):
        # Set the time allowed to win this game.
        GameTimer = threading.Timer(Game_Time, game_time_up)
        GameTimer.start()
        
    def game_time_up():
        # Notify the players that the time has run out, with no one guessing the word.
        # They should be told what the word was, in case they were close.
        print("No one managed to guess Dumathoin's word:", self.secret_word.upper(), "in the allotted time.")
        self.end_game()
   
    def end_game(self):
        # Notify the players that the game is over, and set the Playing value to False.
        self.Playing = False
        print("announce The current game of 'Warps and Wefts' is over.")

    def intro(self):
        # General intro that the game is active, how to get rules information, and basic stats.
        print("announce Dumathoin has a", len(self.secret_word), "letter word in mind. To guess what the word is: guess("")")
        print("announce The secret word is", int(len(self.secret_word)), "letters long, and that's how long your guesses should be.")
        print("announce Whomever can guess Dumathoin's secret word will receive", self.AP_Reward, "AP.")
        print("announce This contest will end in", int(self.Game_Time/60), "minutes.")
        print("announce For rules, type: tell duma WWrules")
        print("announce You may begin guessing.")

    def rules(self, player):
        print("tell", player, "Every secret word is an actual word and has no repeating characters.")
        print("tell", player, "After you make your guess by typing 'tell duma guess your-guess', you will receive a response.")
        print("tell", player, "A 'WARP' means you guessed a correct character in the correct location.")
        print("tell", player, "A 'WEFT' means you guessed a character correctly, but in the wrong position.")
    
    def reward_player(self, player):
        print("The secret word has been discovered:", secret_word.upper())
        print("COMMAND: apoints reward", player, AP_Reward)
        print(player, "has been rewarded with", AP_Reward, "Adventure Points!")
    
    def isograph_check(self, s):
        return len(s) == len(set(s))

    def process_guess_from_player(self, player, player_guess):
        # This will check how many warps are found by comparing guess and secret_word.
        # A 'warp' is defined as the correct letter in the correct position.
        # This function assumes that the length of secret_word and guess are equal.
        # WarpsWefts list holds the results.
        # Should check fitness of guess - alpha, isographic, len compared to secret.
        global secret_word
        global Playing
        if self.Playing:
            if self.isograph_check(player_guess) and player_guess.isalpha() and (len(player_guess.lower()) == len(self.secret_word.lower())):
                Warps = 0
                Wefts = 0
                for i in range(len(player_guess)):
                    if player_guess.lower()[i] == self.secret_word.lower()[i]:
                        Warps += 1
                    elif player_guess.lower()[i] in self.secret_word.lower():
                        Wefts += 1
                if Warps == len(self.secret_word):
                    self.__reward_player(player)
                    self.__end_game()
                else:
                    return (Warps, Wefts)
            elif self.__isograph_check(player_guess) == False:
                print("tell", player, "Your guess should not repeat any letters.")
            elif player_guess.isalpha() == False:
                print("tell", player, "Your guess should only contain letters - no numbers of special characters.")
            elif len(player_guess.lower()) != len(self.secret_word.lower()):
                print("tell", player, "The secret word contains", len(self.secret_word), "letters. Your guess should also have this many letters.")
        else:
            print("tell", player, "There is no game in progress.")
        return

    def return_guess_results_to_player(self, player, guess, answer):
        # answer will be a tuple generated by process_guess_by_player()
        print("tell", player, "your guess:", guess.upper(), "revealed", int(answer[0]), "Warps and", int(answer[1]), "Wefts.")

    def answer(self, player):
        # This will print out the secret_word. For testing purposes.
        print("tell", player, "The secret word is:", self.secret_word)
      
    
# A list of 3 letter words without repeating characters.
# Set as tuple, because it will not be changed during program execution.
words3letters = (
    'abo','abs','aby','ace','act','ado','ads','adz','aft','age','ago',
    'aid','ail','aim','ain','air','ais','ait','alb','ale','alp','als',
    'alt','ami','amp','amu','and','ane','ani','ant','any','ape','apt',
    'arb','arc','are','arf','ark','arm','ars','art','ash','ask','asp',
    'ate','auk','ave','avo','awe','awl','awn','axe','aye','ays','azo',
    'bad','bag','bah','bal','bam','ban','bap','bar','bas','bat','bay',
    'bed','beg','bel','ben','bet','bey','bid','big','bin','bio','bis',
    'bit','biz','boa','bod','bog','bop','bos','bot','bow','box','boy',
    'bra','bro','bud','bug','bum','bun','bur','bus','but','buy','bye',
    'bys','cab','cad','cam','can','cap','car','cat','caw','cay','cel',
    'cep','chi','cig','cis','cob','cod','cog','col','con','cop','cor',
    'cos','cot','cow','cox','coy','coz','cry','cub','cud','cue','cum',
    'cup','cur','cut','cwm','dab','dag','dah','dak','dal','dam','dap',
    'daw','day','deb','del','den','dev','dew','dex','dey','dib','die',
    'dig','dim','din','dip','dis','dit','doc','doe','dog','dol','dom',
    'don','dor','dos','dot','dow','dry','dub','due','dug','duh','dui',
    'dun','duo','dup','dye','ear','eat','eau','ecu','edh','eds','efs',
    'eft','ego','eld','elf','elk','elm','els','emf','ems','emu','end',
    'eng','ens','eon','era','erg','ern','ers','eta','eth','fad','fag',
    'fan','far','fas','fat','fax','fay','fed','feh','fem','fen','fer',
    'fet','feu','few','fey','fez','fib','fid','fie','fig','fil','fin',
    'fir','fit','fix','fiz','flu','fly','fob','foe','fog','foh','fon',
    'fop','for','fou','fox','foy','fro','fry','fub','fud','fug','fun',
    'fur','gab','gad','gae','gal','gam','gan','gap','gar','gas','gat',
    'gay','ged','gel','gem','gen','get','gey','ghi','gib','gid','gie',
    'gin','gip','git','gnu','goa','gob','god','gor','gos','got','gox',
    'goy','gul','gum','gun','gut','guv','guy','gym','gyp','had','hae',
    'hag','haj','ham','hao','hap','has','hat','haw','hay','hem','hen',
    'hep','her','hes','het','hew','hex','hey','hic','hid','hie','him',
    'hin','hip','his','hit','hob','hod','hoe','hog','hon','hop','hot',
    'how','hoy','hub','hue','hug','hum','hun','hup','hut','hyp','ice',
    'ich','ick','icy','ids','ifs','ilk','imp','ink','ins','ion','ire',
    'irk','ism','its','ivy','jab','jag','jam','jar','jaw','jay','jet',
    'jeu','jew','jib','jig','jin','job','joe','jog','jot','jow','joy',
    'jug','jun','jus','jut','kab','kae','kaf','kas','kat','kay','kea',
    'kef','keg','ken','kep','kex','key','khi','kid','kif','kin','kip',
    'kir','kit','koa','kob','koi','kop','kor','kos','kue','lab','lac',
    'lad','lag','lam','lap','lar','las','lat','lav','law','lax','lay',
    'lea','led','leg','lei','lek','let','leu','lev','lex','ley','lez',
    'lib','lid','lie','lin','lip','lis','lit','lob','log','lop','lot',
    'low','lox','lug','lum','luv','lux','lye','mac','mad','mae','mag',
    'man','map','mar','mas','mat','maw','max','may','med','meg','mel',
    'men','met','mew','mho','mib','mid','mig','mil','mir','mis','mix',
    'moa','mob','moc','mod','mog','mol','mon','mop','mor','mos','mot',
    'mow','mud','mug','mun','mus','mut','nab','nae','nag','nah','nam',
    'nap','naw','nay','neb','net','new','nib','nil','nim','nip','nit',
    'nix','nob','nod','nog','noh','nom','nor','nos','not','now','nth',
    'nub','nus','nut','oaf','oak','oar','oat','obe','obi','oca','ode',
    'ods','oes','oft','ohm','ohs','oil','oka','oke','old','ole','oms',
    'one','ons','ope','ops','opt','ora','orb','orc','ore','ors','ort',
    'ose','oud','our','out','ova','owe','owl','own','oxy','pac','pad',
    'pah','pal','pam','pan','par','pas','pat','paw','pax','pay','pea',
    'pec','ped','peg','peh','pen','per','pes','pet','pew','phi','pht',
    'pia','pic','pie','pig','pin','pis','pit','piu','pix','ply','pod',
    'poh','poi','pol','pom','pot','pow','pox','pro','pry','psi','pub',
    'pud','pug','pul','pun','pur','pus','put','pya','pye','pyx','qat',
    'qua','rad','rag','rah','raj','ram','ran','rap','ras','rat','raw',
    'rax','ray','reb','rec','red','ref','reg','rei','rem','rep','res',
    'ret','rev','rex','rho','ria','rib','rid','rif','rig','rim','rin',
    'rip','rob','roc','rod','roe','rom','rot','row','rub','rue','rug',
    'rum','run','rut','rya','rye','sab','sac','sad','sae','sag','sal',
    'sap','sat','sau','saw','sax','say','sea','sec','seg','sei','sel',
    'sen','ser','set','sew','sex','sha','she','shy','sib','sic','sim',
    'sin','sip','sir','sit','six','ska','ski','sky','sly','sob','sod',
    'sol','som','son','sop','sot','sou','sow','sox','soy','spa','spy',
    'sri','sty','sub','sue','sum','sun','sup','suq','syn','tab','tad',
    'tae','tag','taj','tam','tan','tao','tap','tar','tas','tau','tav',
    'taw','tax','tea','ted','teg','tel','ten','tew','the','tho','thy',
    'tic','tie','til','tin','tip','tis','tod','toe','tog','tom','ton',
    'top','tor','tow','toy','try','tsk','tub','tug','tui','tun','tup',
    'tux','twa','two','tye','udo','ugh','uke','ump','uns','upo','ups',
    'urb','urd','urn','use','uta','uts','vac','van','var','vas','vat',
    'vau','vaw','veg','vet','vex','via','vie','vig','vim','vis','voe',
    'vow','vox','vug','wab','wad','wae','wag','wan','wap','war','was',
    'wat','wax','way','web','wed','wen','wet','wha','who','why','wig',
    'win','wis','wit','wiz','woe','wog','wok','won','wop','wos','wot',
    'wry','wud','wye','wyn','xis','yah','yak','yam','yap','yar','yaw',
    'yea','yeh','yen','yep','yes','yet','yew','yid','yin','yip','yob',
    'yod','yok','yom','yon','you','yow','yuk','yum','yup','zag','zap',
    'zax','zed','zek','zig','zin','zip','zit','zoa'
    )
