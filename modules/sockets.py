from module import Module


class Sockets(Module):
    NAME = 'Sockets'

    def __init__(self, *args, **kwargs):
        super(Sockets, self).__init__(*args, **kwargs)

        self.host_players = {}

    def on_started_sockets(self, *args, **kwargs):
        self.host_players = {}

    def on_receive_socket(self, player, host, *args, **kwargs):
        if host not in self.host_players:
            self.host_players[host] = []

        players = self.host_players[host]

        players.append(player)

    def on_ended_sockets(self, *args, **kwargs):
        from settings import IMMORTALS, WHITELIST_HOSTS
        WHITELIST_PLAYERS = IMMORTALS
        for host, players in self.host_players.iteritems():
            skippable = False
            for item in WHITELIST_HOSTS:
                if item in host:
                    skippable = True

            for player in players:
                if player in WHITELIST_PLAYERS:
                    skippable = True

            if skippable:
                continue

            # if len(players) > 1:
                # self.immtalk('{} has multiplayers: {}'.format(
                    # host,
                    # ', '.join(players)
                # ))
