# Adapt MM2k6 RedWizard script to Python.
# This script would act as a 'helper' application for any immortal interested
# in running this kind of IQ. The script would create a new game object that
# would track: players, playerstatus, playerrole, voting, role assignment, etc.
# It would remain up to the IQ running immortal to develop interesting
# night-time deaths, etc.