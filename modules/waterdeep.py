from module import Module
import re


class Waterdeep(Module):
    NAME = 'Waterdeep'

    def __init__(self, *args, **kwargs):
        super(Waterdeep, self).__init__(*args, **kwargs)

        self.mwho_listing = False
        self.mwho_section = "immortals"
        self.mwhos = []
        self.socket_listing = False
        self.sockets = []

    #  -==Immortals==-
    # Name          Level   Wiz   Incog   [ Vnum]
    # Torog          107     0      0      [42197]
    # Ubtao          107     0      0      [56790]
    # Sharess        106     0      0      [68818]
    # Dumatest       108     102     0      [   69]
    # Mielikki       109     0      0      [ 8902]
    # Kelemvor       110     102     0      [   69]
    # Jergal         106     106     0      [95499]
    # Dumathoin      108     102     0      [ 3000]
    # Bahamut        107     102     0      [42197]
    #
    #  -==Mortals==-
    # Name:         Race/Class:   Position:    Level:  %hps  [Vnum]
    # Deckard        Human/Prs    Standing        101    95%   [68746]
    # Khaos          Hflng/Mer    Fighting        101    92%   [33687]
    # Malik          Podkv/Wiz    Standing        101   100%   [20403]
    # Matt           Drow /Lic    Resting         101   100%   [20403]
    # Nox            Dwarf/Wiz    Standing        101   100%   [40000]
    # Balos          Human/Str    Sitting         101   100%   [ 4501]
    # Casper         Mntur/Str    Standing        101    92%   [    2]
    # Umbreon        Drow /War    Standing        9    100%   [ 3005]
    # Relic          Drow /Gla    Standing        101    97%   [    2]
    # Dobu           Human/Mer    Standing        101    81%   [40807]
    # Oreza          Human/Lic    Standing        101    99%   [68610]
    #
    def handle_mwho_line(self, line):
        if not self.mwho_listing and '-==Immortals==-' in line:
            self.mwho_listing = True
            self.mwho_section = "immortals"
            self.mwhos = []
            return

        if not self.mwho_listing:
            return

        if line.startswith("Name"):
            return

        if line.strip() == "":
            if self.mwho_section == "mortals":
                self.bot.trigger("on_mwho", players=self.mwhos)
                self.mwhos = []
                self.mwho_listing = False
                for line in self.mwhos:
                    print(line)
                return
            else:
                return

        if '-==Mortals==-' in line:
            self.mwho_section = "mortals"
            return

        parts = line.split()
        if self.mwho_section == "immortals":
            # Immortal
            # Dumathoin      108     102     0      [ 3000]
            # Sharess        106     0      0      [68818]

            name = parts.pop(0)
            level = int(parts.pop(0).strip())
            wizi = int(parts.pop(0).strip())
            incog = int(parts.pop(0).strip())
            room_vnum = parts.pop(0)
            while "]" not in room_vnum:
                room_vnum += parts.pop(0)
            room_vnum = int(room_vnum.strip("[] "))

            self.mwhos.append({
                "immortal": True,
                "name": name,
                "level": level,
                "wizi": wizi,
                "incog": incog,
                "room_vnum": room_vnum,
            })
        else:
        # Mortal
        # Meja           Elf  /Mnk    Fighting        72    77%   [31701]
            name = parts.pop(0)

            race_class = parts.pop(0)
            while "/" not in race_class:
                race_class += parts.pop(0)
            race_class = race_class.split("/")
            race = race_class[0].strip()
            class_ = race_class[1].strip()

            position = parts.pop(0)
            level = int(parts.pop(0).strip())
            hp_percent = parts.pop(0)
            room_vnum = parts.pop(0)
            while "]" not in room_vnum:
                room_vnum += parts.pop(0)
            room_vnum = int(room_vnum.strip("[] "))

            self.mwhos.append({
                "immortal": False,
                "name": name,
                "race": race,
                "class": class_,
                "position": position,
                "level": level,
                "hp_percent": hp_percent,
                "room_vnum": room_vnum,
            })

    def receive(self, line):
        self.handle_mwho_line(line)

        # Are we currently in, or entering, a socket listing?
        if not self.socket_listing:
            if '[Num Connected_State Login@ Idl] Player Name Host' in line:
                self.socket_listing = True
                self.sockets = []
                self.bot.trigger('on_started_sockets')
        else:
            if ' users' in line:
                self.socket_listing = False

                sockets = self.sockets

                self.sockets = []

                self.bot.trigger('on_ended_sockets', sockets=sockets)
                self.bot.trigger('on_sockets', sockets=sockets)

            elif '[' in line:
                socket_parts = line.split()

                next_part = socket_parts.pop(0)
                if next_part == '[':
                    next_part = socket_parts.pop(0)
                socket_id = int(next_part.strip('['))
                socket_state = socket_parts.pop(0)
                socket_login_time = socket_parts.pop(0)
                socket_idle_minutes = 0

                socket_idle_minutes = 0
                next_part = socket_parts.pop(0)
                while ']' not in next_part :
                    try:
                        socket_idle_minutes = int(next_part.strip(']'))
                    except Exception:
                        pass
                    next_part = socket_parts.pop(0)

                socket_player = socket_parts.pop(0)
                socket_host = socket_parts.pop(0)

                socket = {
                    "id": socket_id,
                    "state": socket_state,
                    "login_time": socket_login_time,
                    "idle_minutes": socket_idle_minutes,
                    "player": socket_player,
                    "host": socket_host,
                }
                self.sockets.append(socket)

                self.bot.trigger(
                    'on_receive_socket',
                    **socket
                )

        results = re.search("^([a-zA-Z]+?) tells you '(.+)'$", line)
        if results:
            self.bot.trigger('receive_tell', player=results.group(1), message=results.group(2))
            self.bot.trigger('on_tell', player=results.group(1), message=results.group(2))
            self.bot.trigger('on_whisper', player=results.group(1), message=results.group(2))

        # --> [NO ACTIVE IMMORTAL] Null is attempting to pray.
        # --> And they said... testing
        pray_user_results = re.search("^--> \[NO ACTIVE IMMORTAL\] ([a-zA-Z]+) is attempting to pray.$", line)
        if pray_user_results:
            self.pray_user = pray_user_results.group(1)

        pray_message_results = re.search("^--\> And they said... (.+)$", line)
        if pray_message_results:
            self.bot.trigger('on_pray', player=self.pray_user, message=pray_message_results.group(1))

        # Null Prays: testing
        pray_results = re.search("^([a-zA-Z]+) Prays: (.+)$", line)
        if pray_results:
            self.bot.trigger('on_pray', player=pray_results.group(1), message=pray_results.group(2))

        # Newbie!
        newbie_results = re.search("^--> ([a-zA-Z]+)\@(.+) new player.$", line)
        if newbie_results:
            self.bot.trigger('on_newbie', player=newbie_results.group(1), host=newbie_results.group(2))

        # Ask channel
        ask_results = re.search("^([a-zA-Z]+) \[Q/A\] Asks '(.+)'$", line)
        if ask_results:
            self.bot.trigger('on_ask', player=ask_results.group(1), message=ask_results.group(2))

        answer_results = re.search("^([a-zA-Z]+) \[Q/A\] Answers '(.+)'$", line)
        if answer_results:
            self.bot.trigger('on_answer', player=answer_results.group(1), message=answer_results.group(2))

        # Login
        login_results = re.search("^--> ([a-zA-Z]+)\@(.+) has connected.$", line)
        if login_results:
            self.bot.trigger('on_login', player=login_results.group(1), host=login_results.group(2))

        # Immtalk, usually
        login_results = re.search("^([a-zA-Z]+): (.+)$", line)
        if login_results:
            name = login_results.group(1)
            if name not in ["Name"]:
                self.bot.trigger('on_immtalk', player=name, message=login_results.group(2))

        # Crosstalk
        login_results = re.search("^\[([a-zA-Z]+) CrossTalks\]: (.+)$", line)
        if login_results:
            self.bot.trigger('on_crosstalk', main_port=True, player=login_results.group(1), message=login_results.group(2))

        # Crosstalk from Builder Port
        login_results = re.search("^\[([a-zA-Z]+) CrossTalks \(Other Port\)\]: (.+)$", line)
        if login_results:
            self.bot.trigger('on_crosstalk', main_port=False, player=login_results.group(1), message=login_results.group(2))
