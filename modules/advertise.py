from module import Module


class Advertise(Module):
    NAME = 'Advertise'

    def init(self):
        self.ticks = 0

    def advertise(self):
        self.gecho('\\n{8[ {cVo{Cte f{Wor Wat{Cerd{ceep! {8-- {Ghttp://vote.waterdeep.info/{8 ]{x')

    def receive_tell(self, player, message):
        if player == 'Kelemvor' and message == 'advertise':
            self.advertise()

    def tick(self):
        self.ticks += 1
        if self.ticks % 3600 == 0:
            self.advertise()
