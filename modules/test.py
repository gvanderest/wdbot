from module import Module
import random


class Test(Module):
    NAME = 'Waterdeep'
    MAX_QUANTITY = 1000

    def init(self):
        self.ticks = 0

    def ready(self):
        current_val = self.get("times_started", 0)
        current_val += 1
        self.log("I'm loaded. {} times and counting.".format(current_val))
        self.set("times_started", current_val)

    def tick(self):
        self.ticks += 1

        if self.ticks % 5 == 0:
            self.writeln("sockets")

        if self.ticks % 20 == 0:
            self.writeln("who")

    def log(self, line):
        # self.echo_at('kelemvor', 'WDBot: ' + line)
        print("LOGGING: " + line)
        self.immtalk('WDBot: ' + line)

    def handle_dice_roll(self, player, args, is_public=False):
        """
        Roll some dice, defaulting to 1d20
        """
        quantity = 1
        size = 20

        # Trying to override default.
        if args:
            dice = args[0]
            parts = dice.split('d')

            try:
                quantity = int(parts[0])
                size = int(parts[1])

            except Exception:
                self.tell(player, 'You have provided an invalid die quantity'
                          ' or size.  Try to tell me: roll 1d6')
                return

        # Too many dice, could cause a weird crash.
        if quantity > self.MAX_QUANTITY:
            self.tell(player, "Sorry, that's too many dice.  Max {}".format(
                self.MAX_QUANTITY
            ))
            return

        # Start rolling some dice.
        total = 0
        for _ in xrange(0, quantity):
            total += random.randint(1, size)

        # Report back to user.
        if is_public:
            self.ooc("{} Requested Rolling {}d{}: {}".format(
                player,
                quantity,
                size,
                total
            ))
        else:
            self.tell(player, "Rolling {}d{}: {}".format(
                quantity,
                size,
                total
            ))

    def handle_pick(self, player, args):
        """Pick a random choice provided."""
        remainder = ' '.join(args).strip()
        if ',' in remainder:
            options = remainder.strip(',').split(',')
        else:
            options = args

        options = map(lambda x: x.strip(), options)

        if not args:
            lines = [
                "You must provide a list of things to pick from.",
                "This list of things can be either separated by spaces or "
                "can be a comma-separated list.",
                "",
                "Example:",
                "tell dumathoin pick apple pie, banana cream pie, cherry pie",
                "tell dumathoin pick apple banana cherry",
            ]
            self.pecho(player, "\\n".join(lines))
            return

        choice = random.choice(options)
        self.ooc("{} Random Pick '{}' from: {}".format(
            player,
            choice,
            ', '.join(options)
        ))

    def handle_wisdom(self, player, args):
        # This will be a Python port of 'WisdomBot'.
        # Feature list:
        # - random quote request by request, or set interval.
        # - quote indexing for specific quote requests and searching
        # - quote submission queue, to be reviewed and approved.
        import random
        quotes = [
            'A hero is the one who knows how to hang on one minute longer. - Norwegian Proverb',
            'You see on the ground a book, titled "The wit and wisdom of Epsilon Moonshade."  Flipping through it, you notice that the pages are blank.',
            'You gotta dance like nobody\'s watching, dream like you will live forever, live like your going to die tomorrow and love like it\'s never going to hurt. -Unknown',
            'You can easily judge the character of a man by how he treats those who can do nothing for him. -James D. Miles',
            'A candle loses nothing by lighting another candle. -Unknown',
            'What lies behind us and what lies before us are tiny matters compared to what lies within us. -Ralph Waldo Emerson',
            'The weak can never forgive. Forgiveness is the attribute of the strong. -Gandhi',
            'The more of the past you carry around, the less of the present there is room for. -Dalton Roberts',
            'Measure wealth not by the things you have, but by the things you have for which you would not take money. -Unknown',
            'To be yourself in a world that is constantly trying to make you something else is the greatest accomplishment. -Ralph Waldo Emerson',
            'To worry is like rocking in a rocking chair. It gives you something to do, but gets you nowhere. -Unknown',
            'If people sat outside and looked at the stars each night, I\'ll bet they\'d live a lot differently. -Calvin, of Calvin and Hobbes',
            'Obstacles are challenges for winners and excuses for losers.',
            'The gem cannot be polished without friction, nor man perfected without trials. -Chinese Proverb',
            'No act of kindness, no matter how small, is ever wasted. -Aesop',
            'It\'s not as hard to die for a friend as it is to find a friend to die for. -Unknown',
            'A man is not finished when he is defeated. He is finished when he quits. -Richard Nixon',
            'We should not let success go to our heads, or our failures go to our hearts. -Unknown',
            'To be a champion you have to believe in yourself when nobody else will. -Sugar Ray Robinson',
            'People are just about as happy as they make up their minds to be. -Abraham Lincoln',
            'Silence is a great peacemaker. -HenryWadsworthLongfellow',
            'There is nothing in which people more betray their character than in what they laugh at. -Johann Wolfgang von Goethe'
        ]

        self.tell(player, quotes[random.randint(0, len(quotes) - 1)])

    def receive_tell(self, player, message):
        self.log("{} whispered: {}".format(player, message))

        parts = message.split()
        print('parts', parts)
        if not parts:
            return

        first_part = parts.pop(0)

        if first_part == "dice" or first_part == "roll" or \
                first_part == "random" or first_part == "publicroll":
            is_public = first_part == "publicroll"
            self.handle_dice_roll(player, parts, is_public)
        elif first_part == "publicpick" or first_part == "pick":
            self.handle_pick(player, parts)
