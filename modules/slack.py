"""
SLACK
Integration for Waterdeep into slack information.
"""
from module import Module
from slacker import Slacker


class Slack(Module):
    def init(self):
        from settings import SLACK_API_KEY, SLACK_ANNOUNCE_CHANNEL, MODULES
        self.connection = Slacker(SLACK_API_KEY)

        self.ticks = 0

        module_names = [cls.__name__ for cls in MODULES]

        message = "I'm loaded, with modules: {}".format(
            ', '.join(module_names) if module_names else 'None'
        )

        self.post(SLACK_ANNOUNCE_CHANNEL, message)

        self.report_connections_on_next_socket = False
        self.report_next_mwho = False

    def ready(self):
        self.report_mwho()

    def post(self, channel, message):
        from settings import SLACK_EMOJI, SLACK_USERNAME

        self.connection.chat.post_message(
            channel,
            message,
            username=SLACK_USERNAME,
            icon_emoji=SLACK_EMOJI,
        )

    def on_pray(self, player, message):
        from settings import SLACK_PRAY_CHANNEL

        message = '*{} prayed:* {}'.format(
            player,
            message
        )

        self.post(SLACK_PRAY_CHANNEL, message)

    def on_immtalk(self, player, message):
        from settings import SLACK_IMMTALK_CHANNEL
        # import settings_login
        # if player == settings_login.USERNAME:
        #     return

        message = '*{} immtalks:* {}'.format(
            player,
            message
        )

        self.post(SLACK_IMMTALK_CHANNEL, message)

    def on_crosstalk(self, player, message, main_port=False):
        from settings import SLACK_CROSSTALK_CHANNEL

        message = '*{} crosstalks ({}):* {}'.format(
            player,
            'main' if main_port else 'builder',
            message,
        )

        self.post(SLACK_CROSSTALK_CHANNEL, message)

    def on_ask(self, player, message):
        from settings import SLACK_ASK_CHANNEL

        message = '*{} asked:* {}'.format(
            player,
            message
        )

        self.post(SLACK_ASK_CHANNEL, message)

    def on_answer(self, player, message):
        from settings import SLACK_ANSWER_CHANNEL

        message = '{} answered: {}'.format(
            player,
            message
        )

        self.post(SLACK_ANSWER_CHANNEL, message)

    def save_and_retrieve_player_host_alts(self, player, host, include_self=False):
        from settings import WEBCLIENT_IPS

        if host in WEBCLIENT_IPS:
            return []

        usernames_by_host = self.get('usernames_by_host', {})
        hosts_by_username = self.get('hosts_by_username', {})

        previous_usernames = usernames_by_host.get(host, [])
        usernames = list(previous_usernames)

        if player not in usernames:
            usernames.append(player)

        user_hosts = hosts_by_username.get(player, [])
        if host not in user_hosts:
            user_hosts.append(host)

        usernames_by_host[host] = usernames
        hosts_by_username[player] = user_hosts

        self.set('usernames_by_host', usernames_by_host)
        self.set('hosts_by_username', hosts_by_username)

        if not include_self:
            if player in previous_usernames:
                previous_usernames.remove(player)

        return previous_usernames

    def on_login(self, player, host):
        from settings import SLACK_LOGIN_CHANNEL
        from settings import WEBCLIENT_IPS

        similars = self.save_and_retrieve_player_host_alts(player, host)

        webclient = host in WEBCLIENT_IPS

        if webclient:
            message = '*{} logged in:* WEBCLIENT'.format(player)

        else:
            message = '*{} logged in:* {} - Similar host players: {}'.format(
                player,
                host,
                ', '.join(similars) if similars else 'None'
            )

        self.post(SLACK_LOGIN_CHANNEL, message)

    def on_newbie(self, player, host):
        from settings import WEBCLIENT_IPS
        from settings import SLACK_NEWBIE_CHANNEL

        similars = self.save_and_retrieve_player_host_alts(
            player,
            host,
            include_self=True
        )

        is_newbie = True
        if player in similars:
            is_newbie = False
            similars.remove(player)

        webclient = host in WEBCLIENT_IPS

        if webclient:
            message = '*Ooh, a {}!* {} - WEBCLIENT'.format(
                'NEWBIE' if is_newbie else 'reroll',
                player
            )

        else:
            message = '*Ooh, a {}!* {} - {} - Similar host players: {}'.format(
                'NEWBIE' if is_newbie else 'reroll',
                player,
                host,
                ', '.join(similars) if similars else 'None'
            )

        self.post(SLACK_NEWBIE_CHANNEL, message)

    def report_sockets(self):
        self.report_connections_on_next_socket = True
        self.writeln("sockets")

    def report_mwho(self):
        self.report_next_mwho = True
        self.writeln("mwho")

    def tick(self):
        self.ticks += 1

        if self.ticks % 600 == 0:
            self.report_mwho()

        if self.ticks % 3600 == 0:
            self.report_sockets()

    def on_mwho(self, players, *args, **kwargs):
        from settings import SLACK_MWHO_CHANNEL

        if self.report_next_mwho:
            self.report_next_mwho = False

            imm_count = 0
            mortal_count = 0
            idle_count = 0
            for player in players:
                if player.get("immortal"):
                    imm_count += 1
                else:
                    mortal_count += 1

                if player.get("room_vnum") == 2:
                    idle_count += 1

            message = "{} players, {} immortals, {} mortals, {} idle".format(
                len(players),
                imm_count,
                mortal_count,
                idle_count,
            )
            self.post(SLACK_MWHO_CHANNEL, message)

    def on_sockets(self, sockets, *args, **kwargs):
        from settings import SLACK_SOCKET_COUNT_CHANNEL

        if self.report_connections_on_next_socket:
            self.report_connections_on_next_socket = False
            message = "There are currently {} sockets connected.".format(
                len(sockets)
            )
            self.post(SLACK_SOCKET_COUNT_CHANNEL, message)

    def on_tell(self, player, message, *args, **kwargs):
        if player == 'Kelemvor' and message == 'sockets':
            self.report_sockets()
