from gevent import monkey, Greenlet
from telnetlib import Telnet
from sys import stdin, stdout
from gevent.fileobject import FileObject

import gevent
import re
import settings, settings_login
import json

monkey.patch_all()


class Bot(Greenlet):
    VERSION = '0.1.0'

    TICK_DELAY = 0.01
    TICK_RESTART = 100
    TICK_STEP = 1
    TICK_METHODS_AND_QUANTITIES = (
        ('tick', 100),
        ('half_tick', 50),
        ('quarter_tick', 25),
        ('tenth_tick', 10),
    )

    THROTTLE_INPUT_LINES = 4
    THROTTLE_INPUT_SECONDS = 1

    import uuid
    READY_TEXT = uuid.uuid4().hex

    ANSI_ESCAPE = re.compile(r'\x1b[^m]*m')

    DATA_FILE_PATH = 'bot-memory.json'

    def __init__(self):
        super(Bot, self).__init__()

        self.telnet = None
        self.running = False
        self.logged_in = False
        self.modules = []
        self.ticks = 0

        self.input_buffer = []
        self.output_buffer = ''

        self.stdin = FileObject(stdin)

        self.data = {}
        self.load_data_from_file()

    def load_data_from_file(self):
        try:
            with open(self.DATA_FILE_PATH, "r") as fh:
                raw_json = fh.read()
                brain = json.loads(raw_json.strip())
                self.data = brain

        # If the file is not found, that's okay.
        except IOError:
            pass

    def save_data_to_file(self):
        with open(self.DATA_FILE_PATH, "w") as fh:
            raw_json = json.dumps(self.data)
            fh.write(raw_json)

    def get(self, key, default=None):
        return self.data.get(key, default)

    def set(self, key, value):
        self.data[key] = value
        self.save_data_to_file()

    def add_module(self, module):
        """ Add a module instance. """
        self.modules.append(module)

    def get_module_names(self):
        """ Get a list of module names. """
        names = []

        for module in self.modules:
            names.append(module.NAME)

        return names

    def login(self):
        self.writeln(settings_login.USERNAME)
        self.writeln(settings_login.PASSWORD, log=False)
        self.writeln("y")
        self.writeln("asdf")
        self.writeln('prompt <prompt>'
                     '{{"r":"%o","e":"%e","z":"%z","t":"%t"}'
                     '</prompt>%c')
        self.writeln('wizi 102')
        self.writeln('wizi')
        self.writeln('tell self {}'.format(self.READY_TEXT))

        self.logged_in = True

    def report_loaded(self):
        module_names = self.get_module_names()

        self.writeln("immtalk WDBot {} loaded with modules: {}".format(
            self.VERSION,
            ', '.join(module_names) if module_names else 'None'
        ))

    def tick(self):
        if self.ticks >= self.TICK_RESTART:
            self.ticks = 0

        self.ticks += self.TICK_STEP

        for method_name, quantity in self.TICK_METHODS_AND_QUANTITIES:
            if self.ticks % quantity == 0:
                self.trigger(method_name)

    def handle_exception(self, e):
        import sys, traceback

        try:
            print("EXCEPTION")
            traceback.print_exc()

            exc_string = str(e)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            lines = traceback.format_tb(exc_traceback)
            exc_line = lines[-1].replace("\n", "").strip()

            if self.logged_in:
                self.writeln("immtalk EXCEPTION: {} - {}".format(
                    exc_string.replace("{", "{{"),
                    exc_line.replace("{", "{{")
                ))

        except Exception:
            pass

    def trigger(self, hook, *args, **kwargs):
        for module in self.modules:
            try:
                if hasattr(module, hook):
		        method = getattr(module, hook)
		        method(*args, **kwargs)
            except Exception, e:
                self.handle_exception(e)

    def handle_user_input(self):
        while self.running:
            line = self.stdin.readline()

            if line:
                self.input_buffer.append(line)

            gevent.sleep(0.001)

    def handle_input_send(self):
        while self.running:
            for x in range(self.THROTTLE_INPUT_LINES):
                if not self.input_buffer:
                    break

                line = self.input_buffer.pop(0)
                self.telnet.write(line + "\n")

            gevent.sleep(self.THROTTLE_INPUT_SECONDS)

    def strip_ansi(self, message):
        return self.ANSI_ESCAPE.sub('', message)

    def handle_output_write(self):
        while self.running:
            if self.output_buffer:
                self.output_buffer = self.output_buffer.replace("\r", "")

                buf = self.output_buffer
                self.output_buffer = ''

                lines = buf.split("\n")

                if lines:
                    # If this content didn't end in a newline, send the
                    # unfinished portion back.
                    if lines[-1]:
                        self.output_buffer += lines.pop()

                    for line in lines:
                        self.handle_output_line(line)

            gevent.sleep(0.01)

    def handle_output_line(self, line):
        """ Output the line to the screen and pass it to the modules. """
        stripped = self.strip_ansi(line)

        # Are there multiple lines?
        lines = line.split("\n")
        if len(lines) > 1:
            for line in lines:
                self.handle_output_line(line)
            return

        print(line)
        self.trigger('receive', stripped)

        if self.READY_TEXT in stripped:
            self.trigger('ready')

    def handle_game_output(self):
        while self.running:
            content = self.telnet.read_some()
            if content:
                self.output_buffer += content
            gevent.sleep(0.001)

    def _run(self):
        self.telnet = Telnet(
            settings.TELNET_HOST,
            settings.TELNET_PORT
        )

        self.login()
        self.trigger('login')

        gevent.sleep(1)

        self.running = True

        self.read_input_thread = Greenlet.spawn(self.handle_user_input)
        self.read_input_thread.start()

        self.send_input_thread = Greenlet.spawn(self.handle_input_send)
        self.send_input_thread.start()

        self.receive_output_thread = Greenlet.spawn(self.handle_game_output)
        self.receive_output_thread.start()

        self.write_output_thread = Greenlet.spawn(self.handle_output_write)
        self.write_output_thread.start()

        self.main_thread()

    def main_thread(self):
        while self.running:
            self.tick()
            gevent.sleep(self.TICK_DELAY)

    def stop(self):
        self.telnet.close()

    def writeln(self, message='', log=True):
        stripped = message.encode('ascii').strip()
        self.input_buffer.append(stripped)

        if log:
            logging = stripped
            logging = logging.replace("\r", "")
            logging = logging.replace("\n", "<NL>")

            print("> " + logging)
