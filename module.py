from gevent import Greenlet
import gevent


class Module(object):
    NAME = '__unnamed__'

    def __init__(self, bot):
        self.bot = bot
        self.writeln = self.bot.writeln
        self.get = self.bot.get
        self.set = self.bot.set

        try:
            self.init()
        except Exception, e:
            self.bot.handle_exception(e)

    def init(self):
        """ On initialize. """
        pass

    def ready(self):
        """ When ready to act on things. """
        pass

    def tick(self):
        """ Ticks often, many times per second. """
        pass

    def delay(self, seconds):
        gevent.sleep(seconds)

    def wait(self, seconds, callback, *args, **kwargs):
        """ Wait a period of time, then run a callback. """
        g = Greenlet(callback, *args, **kwargs)
        g.start_later(seconds)

    def immtalk(self, message):
        """ Use the immtalk channel. """
        self.writeln("immtalk " + message)

    def say(self, message):
        """ Use the say channel. """
        self.writeln("say " + message)

    def gecho(self, message):
        """ Use the gecho channel. """
        self.writeln("gecho " + message)

    def ooc(self, message):
        """ Use the ooc channel. """
        self.writeln("ooc " + message)

    def receive(self, line):
        """ Receive a line from the game. """
        pass

    def pecho(self, targets, message):
        """An alias for echo_at."""
        return self.echo_at(targets, message)

    def echo_at(self, targets, message):
        """
        Send a message to a single or multiple targets via pecho.

        @param {(list|str)} targets
        @param {string} message
        """
        if type(targets) is str:
            targets = [targets]

        for target in targets:
            self.writeln("pecho {} {}".format(target, message))

    def tell(self, targets, message):
        """
        Send a message to a single or multiple targets.

        @param {(list|str)} targets
        @param {string} message
        """

        if type(targets) is str:
            targets = [targets]

        for target in targets:
            self.writeln("tell {} {}".format(target, message))
