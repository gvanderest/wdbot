package com.palmandocean.InstaVue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InstaVueApplication {

	public static void main(String[] args) {
		SpringApplication.run(InstaVueApplication.class, args);
	}

}
