package com.jheilman.openbranches;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenBranchesApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenBranchesApplication.class, args);
	}

}
