/*
 * Copyright 2006 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jheilman;

import com.backblaze.b2.client.contentHandlers.B2ContentFileWriter;
import com.backblaze.b2.client.exceptions.B2Exception;
import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

import com.opensymphony.xwork2.conversion.annotations.Conversion;
import com.opensymphony.xwork2.conversion.annotations.TypeConversion;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.ws.Action;
import com.backblaze.b2.client.B2StorageClient;

/**
 *
 */
@Conversion()
public class IndexAction extends ActionSupport {

    private BackBlazeBucket myBucket;
    private BackBlazeService backBlazeService = new BackBlazeService();

    /**
     * Called by index.jsp for displaying blazeFiles with STRUTS iterator
     * @return List of files in the bucket.
     */
    public List<BackBlazeFile> getBlazeFiles() {
        return myBucket.getFiles();
    }

    public String execute() throws Exception {
        myBucket = backBlazeService.getMyBucket();
        return SUCCESS;
    }

}
