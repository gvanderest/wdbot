WDBot Python Telnet Bot
=======================

Purpose of WDBot
----------------
* Learn to code python a bit more!
* Games that the bot can run in-game like poker, secret word, Red Wizard, etc.
* Statistics gathering (which players killed the most, the least, etc.)
* Some proposed new games - GlobalQuests -- multiple mobs loaded by bot,
* players have set time to find and defeat all the mobs in the list.
* Automatically search for multiplayers and punish
* Track alt characters automatically for us
* Player Voting System
* Procedural Content Generation - Custom Areas for Players
* New player tracking - comparison against known sockets
* Automatic Welcome Notes for New Players?
* Automated Flag Removals, with reflagging that logs reflagged gear, sets hash on item itself?
* Daily, Weekly, Monthly Reporting of Statistics - Currency Flows, Player Activity

Requirements
------------
* WhiteList for incoming tells from approved players, immortals.
* SocketPlayerHistory - perhaps a list of all observed sockets with the players who have used them.
* PlayerSocketHistory - what players have been observed, and what sockets have they used.
