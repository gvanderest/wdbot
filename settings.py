"""
SETTINGS
"""
from modules.waterdeep import Waterdeep
from modules.test import Test
from modules.sockets import Sockets
from modules.advertise import Advertise
from modules.slack import Slack

import settings_login

TELNET_HOST = 'waterdeep.org'
TELNET_PORT = 4200

IMMORTALS = [
    'Torog',
    'Kelemvor',
    'Mielikki',
    'Dumathoin',
    'Jergal',
    'Sune',
    'Kord',
    'Nisstyre',
    'Bane',
    'Tymora',
    'Epsilon',
    'Fenmarel',
    'Myrkul',
    'Mask',
    'Vorcet',
    'Malar',
    'Amaunator',
    'Aeris',
    'Istus'
]
# WHITELIST_HOSTS = [
    # 'telus.net'
# ]

MODULES = [
    Waterdeep,
    Test,
    # Sockets,
    Advertise,
    Slack
]

SLACK_API_KEY = 'xoxp-17298791829-17302845894-27923916144-4b5e8d50c2'
SLACK_USERNAME = settings_login.USERNAME
SLACK_EMOJI = ':imp:'

SLACK_ANNOUNCE_CHANNEL = '#bots'
SLACK_LOGIN_CHANNEL = '#wiznet'
SLACK_PRAY_CHANNEL = '#general'
SLACK_ASK_CHANNEL = '#ask'
SLACK_ANSWER_CHANNEL = '#ask'
SLACK_NEWBIE_CHANNEL = '#general'
SLACK_CROSSTALK_CHANNEL = '#builders'
SLACK_IMMTALK_CHANNEL = '#immtalk'
SLACK_SOCKET_COUNT_CHANNEL = '#wiznet'
SLACK_MWHO_CHANNEL = '#census'

WEBCLIENT_IPS = ['198.199.93.118']
